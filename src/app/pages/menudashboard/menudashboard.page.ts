import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menudashboard',
  templateUrl: './menudashboard.page.html',
  styleUrls: ['./menudashboard.page.scss'],
})
export class MenudashboardPage implements OnInit {

  menu = [
    {
      title: "Note",
      route: 'noteslist',
      icon: ""
    },
    {
      title: "budget",
      route: 'budgetdashboard',
      icon: ""
    },
    {
      title: "Reminder",
      route: "reminder",
      icon: ""
    }

  ]
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }
  routeing(data) {
    this.router.navigateByUrl(data.route);
  }

}
