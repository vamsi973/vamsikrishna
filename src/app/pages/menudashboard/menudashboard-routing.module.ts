import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenudashboardPage } from './menudashboard.page';

const routes: Routes = [
  {
    path: '',
    component: MenudashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenudashboardPageRoutingModule {}
