import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenudashboardPage } from './menudashboard.page';

describe('MenudashboardPage', () => {
  let component: MenudashboardPage;
  let fixture: ComponentFixture<MenudashboardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenudashboardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenudashboardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
