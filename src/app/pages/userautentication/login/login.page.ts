import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from '../../../providers/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  login = new FormGroup({
    crdentialId: new FormControl("", Validators.compose([Validators.required])),
    password: new FormControl("", Validators.compose([Validators.required])),
  });
  constructor(
    private authService: AuthService,
    public router: Router
  ) { }

  ngOnInit() { }

  loginSumbit() {
    console.log(this.login.value);
    this.authService.login(this.login.value).subscribe((data) => {
      console.log(data);
      if (data.success) {
        this.router.navigateByUrl('/menudashboard')
      }
    })
    
  }

}
