import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BudgetDashboardPageRoutingModule } from './budget-dashboard-routing.module';

import { BudgetDashboardPage } from './budget-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BudgetDashboardPageRoutingModule
  ],
  declarations: [BudgetDashboardPage]
})
export class BudgetDashboardPageModule {}
