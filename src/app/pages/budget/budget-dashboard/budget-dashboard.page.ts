import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-budget-dashboard',
  templateUrl: './budget-dashboard.page.html',
  styleUrls: ['./budget-dashboard.page.scss'],
})
export class BudgetDashboardPage implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }
  createBudget() {
    console.log("navigate ")
    this.router.navigateByUrl('/createbudget');
  }

}
