import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BudgetDashboardPage } from './budget-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: BudgetDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BudgetDashboardPageRoutingModule {}
