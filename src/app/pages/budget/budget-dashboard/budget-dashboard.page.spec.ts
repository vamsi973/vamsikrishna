import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BudgetDashboardPage } from './budget-dashboard.page';

describe('BudgetDashboardPage', () => {
  let component: BudgetDashboardPage;
  let fixture: ComponentFixture<BudgetDashboardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetDashboardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BudgetDashboardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
