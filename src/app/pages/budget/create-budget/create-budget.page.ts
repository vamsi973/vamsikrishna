
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BudgetService } from '../../../providers/budget.service';


@Component({
  selector: 'app-create-budget',
  templateUrl: './create-budget.page.html',
  styleUrls: ['./create-budget.page.scss'],
})
export class CreateBudgetPage implements OnInit {

  spendingOptions = [
    {
      "type": "Income"
    },
    {
      "type": "Lending"
    },
    {
      "type": "Spending"
    }
  ]
  constructor(
    public budgetService: BudgetService
  ) { }
  budget: FormGroup = new FormGroup(
    {
      title: new FormControl('', [Validators.required]),
      amount: new FormControl('', [Validators.required]),
      spendingType: new FormControl('', [Validators.required]),
      spentDate: new FormControl('', [Validators.required]),
    }
  )

  ngOnInit() { }

  createSpending() {
    if (this.budget.valid) {
      this.budgetService.createSpending(this.budget.value).subscribe(
        (spendingRecord) => {
          console.log(spendingRecord);
        }
      )
    }
  }


}
