import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoteslistPage } from './noteslist.page';

const routes: Routes = [
  {
    path: '',
    component: NoteslistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoteslistPageRoutingModule {}
