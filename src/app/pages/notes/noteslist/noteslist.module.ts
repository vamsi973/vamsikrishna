import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoteslistPageRoutingModule } from './noteslist-routing.module';

import { NoteslistPage } from './noteslist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoteslistPageRoutingModule
  ],
  declarations: [NoteslistPage]
})
export class NoteslistPageModule {}
