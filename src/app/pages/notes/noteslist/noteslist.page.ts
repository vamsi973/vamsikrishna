import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NotesService } from '../../../providers/notes.service';


@Component({
  selector: 'app-noteslist',
  templateUrl: './noteslist.page.html',
  styleUrls: ['./noteslist.page.scss'],
})
export class NoteslistPage implements OnInit {
  storyList = [];
  constructor(
    private router: Router,
    private notesService: NotesService
  ) { }

  ngOnInit() {
    this.notesList();
  }

  addANote() {
    this.router.navigateByUrl('/noteCreate')
  }

  notesList() {
    this.notesService.notesList().subscribe((noteList) => {
      this.storyList = noteList.data;
      console.log(this.notesList);
    })
  }
}
