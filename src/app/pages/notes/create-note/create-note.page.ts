import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotesService } from '../../../providers/notes.service';


@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.page.html',
  styleUrls: ['./create-note.page.scss'],
})
export class CreateNotePage implements OnInit {

  constructor(
    public noteService: NotesService
  ) { }

  ngOnInit() {
  }

  createNote: FormGroup = new FormGroup(
    {
      title: new FormControl('', [Validators.required]),
      url: new FormControl('', [Validators.required]),

    }
  )
  createNoteSubmit() {
    console.log('rjowier')
    this.noteService.noteCreate(this.createNote.value).subscribe((createdNote) => {
      console.log(createdNote);
    })
  }

}
