import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './pages/guard/auth.guard';
import { CheckTutorial } from './providers/check-tutorial.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tutorial',
    pathMatch: 'full'
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./pages/support/support.module').then(m => m.SupportModule)
  },
  // {
  //   path: 'login',
  //   loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  // },
  // {
  //   path: 'signup',
  //   loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignUpModule)
  // },
  {
    path: 'app',
    loadChildren: () => import('./pages/tabs-page/tabs-page.module').then(m => m.TabsModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./pages/tutorial/tutorial.module').then(m => m.TutorialModule),
    canLoad: [CheckTutorial]
  },
  {
    path: 'menudashboard',
    loadChildren: () => import('./pages/menudashboard/menudashboard.module').then(m => m.MenudashboardPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'noteslist',
    loadChildren: () => import('./pages/notes/noteslist/noteslist.module').then(m => m.NoteslistPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'budgetdashboard',
    loadChildren: () => import('./pages/budget/budget-dashboard/budget-dashboard.module').then(m => m.BudgetDashboardPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'noteCreate',
    loadChildren: () => import('./pages/notes/create-note/create-note.module').then(m => m.CreateNotePageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'createbudget',
    loadChildren: () => import('./pages/budget/create-budget/create-budget.module').then( m => m.CreateBudgetPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/userautentication/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/userautentication/register/register.module').then(m => m.RegisterPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
