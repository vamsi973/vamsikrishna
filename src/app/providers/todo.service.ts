import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config } from '../pages/helperData/config';


@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(
    private httpClient : HttpClient
  ) { }

  register(formData):Observable<any>{
    return this.httpClient.post(config.api+"auth/signup" ,formData)
  }
}
