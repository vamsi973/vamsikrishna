import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config } from '../pages/helperData/config';


@Injectable({
  providedIn: 'root'
})
export class BudgetService {

  constructor(private httpClient: HttpClient) { }
  createSpending(formData): Observable<any> {
    return this.httpClient.post(config.api + "budget/insertSpending", formData)
  }
  spendingsSummary(): Observable<any> {
    return this.httpClient.get(config.api + "budget/budgetSummary")
  }
}
