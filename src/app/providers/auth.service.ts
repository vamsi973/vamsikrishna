import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { tap } from 'rxjs/operators';

import { config } from '../pages/helperData/config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token: any;
  constructor(
    private httpClient: HttpClient,
    
  ) { }

  register(formData): Observable<any> {
    return this.httpClient.post(config.api + "auth/userRegister", formData)
  }


  login(loginData): Observable<any> {
    return this.httpClient.post(config.api + 'auth/login', loginData
    ).pipe(
      tap((token) => {
        console.log(token)
        this.storageUpdate(token);
        this.token = token['token'];
        this.isLoggedIn = true;
        return token;
      }),
    );
  }
  logout() {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"] + " " + this.token["access_token"]
    });
    return this.httpClient.get(config.api + 'auth/logout', { headers: headers })
      .pipe(
        tap(data => {
          // this.storage.remove("token");
          this.isLoggedIn = false;
          delete this.token;
          return data;
        })
      )
  }


  storageUpdate(tokenData) {
    localStorage.setItem('token', tokenData.token);
    localStorage.setItem('user_id', tokenData.data[0].user_id);
    localStorage.setItem('emil', tokenData.data[0].email);
    localStorage.setItem('firstName', tokenData.data[0].name);

  }

  // user() {
  //   const headers = new HttpHeaders({
  //     'Authorization': this.token["token_type"]+" "+this.token["access_token"]
  //   });
  //   return this.http.get<User>(this.env.API_URL + 'auth/user', { headers: headers })
  //   .pipe(
  //     tap(user => {
  //       return user;
  //     })
  //   )
  // }
  loggedIn() {
    console.log('efe');
    if (localStorage.getItem('token')) {
      console.log('token')
      this.token = localStorage.getItem('token');
      if (this.token != null) {
        this.isLoggedIn = true;
        
      } else {
        this.isLoggedIn = false;
      }
      console.log(this.isLoggedIn)
      return this.isLoggedIn
    }
  }
}
