import { HttpEvent, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
        Authorization: `${localStorage.getItem('token')}`,
        headers: 'Content-Type, application/json'
      }
    })

    return next.handle(req).pipe(
      tap(ele => {
        // console.log(ele,186354)
        if (ele instanceof HttpResponse) {
          if (ele.body && ele.body.msg == "Invalid Authorization!") {
            // console.log("invalid");
            // localStorage.clear();
            // this.auth.logout();
            // this.router.navigateByUrl('/login');
          } else {
            // console.log("valid request");
          }
        }
      })
    );
  }
  constructor(
    private router: Router,
    // public auth: AuthServicesService
  ) {

  }


}
