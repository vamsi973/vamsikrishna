import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config } from '../pages/helperData/config';


@Injectable({
  providedIn: 'root'
})
export class NotesService {

  constructor(private httpClient: HttpClient) { }
  noteCreate(formData): Observable<any> {
    return this.httpClient.post(config.api + "notes/createNote", formData)
  }
  notesList(): Observable<any> {
    return this.httpClient.get(config.api + "notes/notesList")
  }
}
